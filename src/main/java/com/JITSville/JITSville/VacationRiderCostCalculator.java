package com.JITSville.JITSville;

public class VacationRiderCostCalculator implements PassengerType {

	public TripSummary gettripSummary(Passenger passenger) {
		// TODO Auto-generated method stub
		VisitorTripDetails visitorTripDetails;
		if (passenger.getPriceFactor() < 4 || passenger.getPriceFactor() > 4000) {
			throw new IllegalArgumentException("Miles should be greater than 4 and less than 4000");
		} else {
			double cost = passenger.getPriceFactor() * passenger.getRateFactor();
			visitorTripDetails = new VisitorTripCalculator().getVisitorTripInformation(passenger);
			return new TripSummary(cost, visitorTripDetails.getNewsPaperCount(), visitorTripDetails.getMealsCount());
		}
	}
}
