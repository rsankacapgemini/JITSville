package com.JITSville.JITSville;

public class FrequentRiderCostCalculator implements PassengerType {

	
	public TripSummary gettripSummary(Passenger passenger) {
		// TODO Auto-generated method stub
		VisitorTripDetails visitorTripDetails;
		double cost = (passenger.getPriceFactor() * passenger.getRateFactor())*.9;
		visitorTripDetails = new VisitorTripCalculator().getVisitorTripInformation(passenger);
		return new TripSummary(cost, visitorTripDetails.getNewsPaperCount(), visitorTripDetails.getMealsCount());	}

}
