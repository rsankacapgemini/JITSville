package com.JITSville.JITSville;

public class VisitorTripCalculator implements IVisitorTrip {

	
	public VisitorTripDetails getVisitorTripInformation(Passenger passenger) {
		// TODO Auto-generated method stub
		int newsPaper = 0;
		int mealsCount = 0;
		if (passenger.isNewsPaperRequested()) {
			newsPaper = 1;
		}
		if (passenger.getPassengerType() == Type.Vaction) {
			mealsCount = (passenger.getPriceFactor() / 100) + 1;
		}

		return new VisitorTripDetails(newsPaper, mealsCount);
	}

}
