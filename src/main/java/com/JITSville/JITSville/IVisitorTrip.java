package com.JITSville.JITSville;

public interface IVisitorTrip {

	VisitorTripDetails getVisitorTripInformation(Passenger passenger);
}
