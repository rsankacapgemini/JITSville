package com.JITSville.JITSville;

import java.util.ArrayList;
import java.util.List;

public class Tester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		List<Passenger> passengers = addPassengers();
		processTripDetails(passengers);
		

	}

	private static void processTripDetails(List<Passenger> passengers) {
		// TODO Auto-generated method stub
		Context context;
		TripSummary tripSummary;
		double totalCost = 0;
		int meals = 0, newsPapers = 0;
		for(Passenger passenger:passengers) {
			if(passenger.getPassengerType() == Type.Commute) {
				context = new Context(new CommuteCostCalculator());
				tripSummary = context.getTripSummary(passenger);
				meals+=tripSummary.getMealsCount();
				newsPapers+=tripSummary.getNewsPapersCount();
				totalCost+=tripSummary.getTripCost();
				System.out.println(tripSummary);
			} else if(passenger.getPassengerType() == Type.FrequentRider) {
				context = new Context(new CommuteCostCalculator());
				tripSummary = context.getTripSummary(passenger);
				meals+=tripSummary.getMealsCount();
				newsPapers+=tripSummary.getNewsPapersCount();
				totalCost+=tripSummary.getTripCost();
				System.out.println(tripSummary);
			} else if(passenger.getPassengerType() == Type.Vaction) {
				context = new Context(new CommuteCostCalculator());
				tripSummary = context.getTripSummary(passenger);
				meals+=tripSummary.getMealsCount();
				newsPapers+=tripSummary.getNewsPapersCount();
				totalCost+=tripSummary.getTripCost();
				System.out.println(tripSummary);
			}
		}
		System.out.println("Total Trip Cost");
		System.out.println("********************");
		System.out.println(getTotalTripCost(totalCost,newsPapers,meals));
		
	}

	private static TripSummary getTotalTripCost(double totalCost, int newsPapers, int meals) {
		// TODO Auto-generated method stub
		return new TripSummary(totalCost, newsPapers, meals);
	}

	private static List<Passenger> addPassengers() {
		// TODO Auto-generated method stub
		List<Passenger> passengers = new ArrayList<Passenger>();
		passengers.add(new Passenger("rohit1", Type.FrequentRider, 3, false));
		passengers.add(new Passenger("rohit2", Type.FrequentRider, 5, false));
		passengers.add(new Passenger("rohit3", Type.Commute, 4, true));
		passengers.add(new Passenger("rohit4", Type.Vaction, 90, false));
		passengers.add(new Passenger("rohit5", Type.Vaction, 199, true));
		return passengers;
	}
	

}
