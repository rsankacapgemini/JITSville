package com.JITSville.JITSville;

public class Context {

	private PassengerType passengerType;

	public Context(PassengerType passengerType) {
		super();
		this.passengerType = passengerType;
	}

	public TripSummary getTripSummary(Passenger passenger) {
		return passengerType.gettripSummary(passenger);
	}
}
