package com.JITSville.JITSville;

public class TripSummary {

	private double tripCost;
	private int newsPapersCount;
	private int mealsCount;

	public TripSummary(double tripCost, int newsPapersCount, int mealsCount) {
		super();
		this.tripCost = tripCost;
		this.newsPapersCount = newsPapersCount;
		this.mealsCount = mealsCount;
	}

	public double getTripCost() {
		return tripCost;
	}

	public int getNewsPapersCount() {
		return newsPapersCount;
	}

	public int getMealsCount() {
		return mealsCount;
	}

	@Override
	public String toString() {
		return "TripSummary [tripCost=" + tripCost + ", newsPapersCount=" + newsPapersCount + ", mealsCount="
				+ mealsCount + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mealsCount;
		result = prime * result + newsPapersCount;
		long temp;
		temp = Double.doubleToLongBits(tripCost);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TripSummary other = (TripSummary) obj;
		if (mealsCount != other.mealsCount)
			return false;
		if (newsPapersCount != other.newsPapersCount)
			return false;
		if (Double.doubleToLongBits(tripCost) != Double.doubleToLongBits(other.tripCost))
			return false;
		return true;
	}

}
