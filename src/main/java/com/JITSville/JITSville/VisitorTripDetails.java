package com.JITSville.JITSville;

public class VisitorTripDetails {
	private int newsPaperCount;
	private int mealsCount;
	public VisitorTripDetails(int newsPaperCount, int mealsCount) {
		super();
		this.newsPaperCount = newsPaperCount;
		this.mealsCount = mealsCount;
	}
	public int getNewsPaperCount() {
		return newsPaperCount;
	}
	public int getMealsCount() {
		return mealsCount;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mealsCount;
		result = prime * result + newsPaperCount;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisitorTripDetails other = (VisitorTripDetails) obj;
		if (mealsCount != other.mealsCount)
			return false;
		if (newsPaperCount != other.newsPaperCount)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "VisitorTripDetails [newsPaperCount=" + newsPaperCount + ", mealsCount=" + mealsCount + "]";
	}
	

}
