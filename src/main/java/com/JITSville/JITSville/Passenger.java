package com.JITSville.JITSville;

public class Passenger {

	private String passegerName;
	private Type passengerType;
	private int priceFactor;
	private boolean newsPaperRequested;
	private final double rateFactor = 0.5;

	public String getPassegerName() {
		return passegerName;
	}

	public void setPassegerName(String passegerName) {
		this.passegerName = passegerName;
	}

	public Type getPassengerType() {
		return passengerType;
	}

	public void setPassengerType(Type passengerType) {
		this.passengerType = passengerType;
	}

	public int getPriceFactor() {
		return priceFactor;
	}

	public double getRateFactor() {
		return rateFactor;
	}

	public boolean isNewsPaperRequested() {
		return newsPaperRequested;
	}

	public void setNewsPaperRequested(boolean newsPaperRequested) {
		this.newsPaperRequested = newsPaperRequested;
	}

	public Passenger(String passegerName, Type passengerType, int priceFactor, boolean newsPaperRequested) {
		super();
		this.passegerName = passegerName;
		this.passengerType = passengerType;
		this.priceFactor = priceFactor;
		this.newsPaperRequested = newsPaperRequested;
	}

	public void setPriceFactor(int priceFactor) {
		this.priceFactor = priceFactor;
	}

	public Passenger() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Passenger [passegerName=" + passegerName + ", passengerType=" + passengerType + ", priceFactor="
				+ priceFactor + ", newsPaperRequested=" + newsPaperRequested + ", rateFactor=" + rateFactor + "]";
	}

}
