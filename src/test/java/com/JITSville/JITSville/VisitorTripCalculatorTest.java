package com.JITSville.JITSville;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
@RunWith(Parameterized.class)
public class VisitorTripCalculatorTest {

	private VisitorTripDetails expectedValue;
	private Passenger passenger;
	private VisitorTripCalculator visitorTripCalculator;

	public VisitorTripCalculatorTest(VisitorTripDetails expectedValue, Passenger passenger) {
		super();
		this.expectedValue = expectedValue;
		this.passenger = passenger;
	}

	@Before
	public void setup() {
		visitorTripCalculator = new VisitorTripCalculator();
	}

	@Test
	public void testGettripSummary() {
		assertEquals(expectedValue, visitorTripCalculator.getVisitorTripInformation(passenger));
		// assertSame(expectedValue, context.getTripSummary(passenger));
	}

	@Parameters
	public static Collection<Object[]> testData() {
		Object[][] data = new Object[][] {
				{ new VisitorTripDetails(1, 0), new Passenger("rohit3", Type.Commute, 4, true) },
				{ new VisitorTripDetails(0, 21), new Passenger("rohit3", Type.Vaction, 2000, false) } };
		return Arrays.asList(data);
	}
}
