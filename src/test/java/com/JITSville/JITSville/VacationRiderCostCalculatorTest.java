package com.JITSville.JITSville;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class VacationRiderCostCalculatorTest {
	private TripSummary expectedValue;
	private Passenger passenger;
	private Context context;

	public VacationRiderCostCalculatorTest(TripSummary expectedValue, Passenger passenger) {
		super();
		this.expectedValue = expectedValue;
		this.passenger = passenger;
	}

	@Before
	public void setup() {
		context = new Context(new VacationRiderCostCalculator());
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testGettripSummary() {
		if (passenger.getPriceFactor() > 4000 || passenger.getPriceFactor() < 4) {
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("Miles should be greater than 4 and less than 4000");
			context.getTripSummary(passenger);
		}
		assertEquals(expectedValue, context.getTripSummary(passenger));


		//assertSame(expectedValue, context.getTripSummary(passenger));
	}

	@Parameters
	public static Collection<Object[]> testData() {
		Object[][] data = new Object[][] {
				{ new TripSummary(2.0, 1, 1), new Passenger("rohit3", Type.Vaction, 4, true) },
				{ new TripSummary(2.0, 1, 1), new Passenger("rohit3", Type.Vaction, 3, true) },
				{ new TripSummary(1720, 0, 35), new Passenger("rohit3", Type.Vaction, 3440, false) },
				{ new TripSummary(10.0, 0, 0), new Passenger("rohit3", Type.Vaction, 4001, false) } };
		return Arrays.asList(data);
	}
}