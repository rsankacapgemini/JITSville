package com.JITSville.JITSville;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class FrequentRiderCostCalculatorTest {
	private TripSummary expectedValue;
	private Passenger passenger;
	private Context context;

	public FrequentRiderCostCalculatorTest(TripSummary expectedValue, Passenger passenger) {
		super();
		this.expectedValue = expectedValue;
		this.passenger = passenger;
	}

	@Before
	public void setup() {
		context = new Context(new FrequentRiderCostCalculator());
	}

	@Test
	public void testGettripSummary() {
		assertEquals(expectedValue, context.getTripSummary(passenger));
		// assertSame(expectedValue, context.getTripSummary(passenger));
	}

	@Parameters
	public static Collection<Object[]> testData() {
		Object[][] data = new Object[][] {
				{ new TripSummary(1.8, 1, 0), new Passenger("rohit3", Type.Commute, 4, true) },
				{ new TripSummary(9.0, 0, 0), new Passenger("rohit3", Type.Commute, 20, false) } };
		return Arrays.asList(data);
	}
}
